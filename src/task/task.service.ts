import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Task } from './entities/task.entity';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task) private readonly taskRepository: Repository<Task>,
  ) {}

  async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
    const task: Task = new Task();
    task.title = createTaskDto.title;
    task.description = createTaskDto.description;
    task.dueDate = createTaskDto.dueDate;
    task.categoryId = createTaskDto.categoryId;
    return this.taskRepository.save(task);
  }

  async findAllTasks(page: number = 1, limit: number = 10): Promise<Task[]> {
    const skip = (page - 1) * limit;
    return this.taskRepository.find({
      skip,
      take: limit,
    });
  }

  async findTasksByCategory(categoryId: string): Promise<Task[]> {
    return this.taskRepository.find({ where: { categoryId } });
  }

  async findOneTask(id: string): Promise<Task | undefined> {
    return this.taskRepository.findOne({ where: { id } });
  }

  async updateTask(
    id: string,
    updateTaskDto: UpdateTaskDto,
  ): Promise<Task | undefined> {
    const task = await this.findOneTask(id);

    if (!task) {
      return undefined;
    }
    task.title = updateTaskDto.title;
    task.description = updateTaskDto.description;
    task.dueDate = updateTaskDto.dueDate;
    task.categoryId = updateTaskDto.categoryId;

    return this.taskRepository.save(task);
  }

  async removeTask(id: string): Promise<void> {
    await this.taskRepository.delete(id);
  }
}
