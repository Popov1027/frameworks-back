import { Task } from '../entities/task.entity';

export interface TasksInterface {
  tasks: Task[];
  total: number;
  skip: number;
  limit: number;
}
