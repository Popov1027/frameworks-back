import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { Login } from './entities/login.entity';
import { JwtPayload } from 'jsonwebtoken';
import { LoginDto } from './dto/login.dto';

@Injectable()
export class LoginService {
  constructor(
    @InjectRepository(Login)
    private readonly userRepository: Repository<Login>,
    private readonly jwtService: JwtService,
  ) {}

  async login(user: LoginDto) {
    const payload: JwtPayload = { email: user.email };
    return {
      token: this.jwtService.sign(payload),
    };
  }
}
